import datetime

import mysql.connector


def create_log_table():
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    # cursor.execute("""  DROP TABLE IF EXISTS log;""")
    # cnx.commit()
    cursor.execute("""  CREATE TABLE IF NOT EXISTS log (
                            id INT auto_increment,
                            petition_id INT,
                            title VARCHAR(200),
                            timestamp TIMESTAMP,
                            url VARCHAR(200),
                            supporters INT,
                            goal INT,
                            FOREIGN KEY(petition_id) REFERENCES petitions(id),
                            PRIMARY KEY (id)
                            );""")
    cnx.commit()
    cursor.close()
    cnx.close()


def insert_into_log_table(petitions_list):
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    current_timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    for pet in petitions_list:
        cursor.execute("""  INSERT INTO log
                            (petition_id, title, timestamp, url, supporters, goal)
                            VALUES
                            (%s, %s, %s, %s, %s, %s)""",
                       (pet.id, pet.title, current_timestamp, pet.url, pet.supporters, pet.goal))
    cnx.commit()
    cursor.close()
    cnx.close()
