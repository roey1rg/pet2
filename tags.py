import mysql.connector


tags = ['Animal Rights', "Children's Rights", 'Civil Rights', 'Corporate Accountability', 'Education',
        'Environment', 'Health', 'Human Rights', 'International Development', 'LGBTQ Rights', 'Media,'
        'Arts, Culture', 'Politics', 'Reproductive Rights', 'Wildlife', "Women's Rights", 'Hottest Petitions',
        'Community Petitions', 'Nonprofit Petitions']
tags = [[tag] for tag in tags]

def create_tags_table():
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.execute("""  CREATE TABLE IF NOT EXISTS tags (
                            id INT auto_increment,
                            tag VARCHAR(40),
                            PRIMARY KEY (id)
                            );""")
    cnx.commit()
    cursor.executemany("""  INSERT INTO tags
                            (tag)
                            VALUES
                            (%s)""", tags)
    cnx.commit()
    cursor.close()
    cnx.close()
