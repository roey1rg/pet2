import datetime

import mysql.connector

from petition import Petition


def deactivate_petitions(petition_ids):
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.execute(
        """  update petitions set active = 0 where id in (%s);""" % ','.join(str(i) for i in petition_ids))
    cnx.commit()
    cursor.close()
    cnx.close()


def create_petitions_table():
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.execute("""  CREATE TABLE IF NOT EXISTS petitions (
                        id INT auto_increment,
                        title VARCHAR(200) UNIQUE,
                        last_update_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        when_added_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                        active tinyint default 1,
                        url VARCHAR(200),
                        content text,
                        before_adding_tags BOOLEAN DEFAULT TRUE,
                        has_sentiment BOOLEAN DEFAULT TRUE,
                        PRIMARY KEY (id)
                        );""")
    cnx.commit()
    cursor.close()
    cnx.close()


def insert_into_petitions_table(petition_list):
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()

    current_timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    for pet in petition_list:
        cursor.execute("""  INSERT IGNORE INTO petitions
                            (title, url, when_added_timestamp, content)
                            VALUES (%s,%s,%s, %s);""",
                       (pet.title, pet.url, current_timestamp, pet.content))
    cnx.commit()
    cursor.close()
    cnx.close()


def get_list_of_petitions_from_petitions_table():
    """this functions returns a list of tuples, each tuple have url at the first index
    and a boolean indicating if the url has not scraped yet at the second index"""
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.execute("""  SELECT title, url, before_adding_tags, id FROM petitions where active = 1;""")
    result = []
    for pet_tup in cursor.fetchall():
        result.append(Petition(title=pet_tup[0], url=pet_tup[1],
                               before_adding_tags=bool(pet_tup[2]), petition_id=pet_tup[3]))
    cursor.close()
    cnx.close()
    return result


def update_petition_content(pre_tagged_petitions):
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    for petition in pre_tagged_petitions:
        # update before_frist_scrape column in 'petitions' to False
        cursor.execute("""  UPDATE petitions
                            SET content = %s
                            WHERE id = %s;""", (petition.content, petition.id,))
        cnx.commit()
