import click
import mysql.connector

import nlp_api
from log import create_log_table, insert_into_log_table
from petition_tag import create_petition_tag_table, insert_into_petition_tag_table
from petitions import create_petitions_table, insert_into_petitions_table, get_list_of_petitions_from_petitions_table, \
    update_petition_content, deactivate_petitions
from scrape_featured import collect_featured, init_petitions
from sentiments import create_sentiment_table
from tags import create_tags_table


# TODO: try using "is_flag" where applicable
@click.command()
@click.option("--feed_name", default="hottest", type=click.Choice(["hottest", "nonprofit", "activist"]),
              help='get top featured petitions in one of three feeds')
@click.option("--headless", type=bool, default=True, help='Use "False" to open browser windows when interacting with '
                                                          'web pages')
@click.option("--limit", type=int, default=None, help='Number of max petitions to get from the site')
@click.option("--scrape", type=bool, default=False, help='Use "True" to scrape site')
@click.option("--drop", type=bool, default=False, help='Use "True" to drop the tables, if exist')
@click.option("--create", type=bool, default=False, help='Use "True" to create the db and tables, if not already exist')
@click.option("--update", type=bool, default=False, help='Use "True" to update petitions in db without adding more '
                                                         'petitions')
@click.option("--sentiment", type=bool, default=False, help='Use "True" to run sentiment analysis')
def main(feed_name, limit, headless, scrape, drop, create, update, sentiment):
    if drop:
        drop_tables()

    if create:
        create_database()
        drop_tables()
        create_tables()
    # inserting the list of petitions into 'petitions' table

    if scrape:
        petition_titles_urls = collect_featured(feed_name, limit=limit, headless=headless)  # returns generator
        petitions = init_petitions(petition_titles_urls)  # returns generator
        insert_into_petitions_table(petitions)

    if update:
        update_tables()

    if sentiment:
        nlp_api.main()


def create_database():
    """ Creating database if not exists """
    print("Creating database if not exists")
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.execute("create database if not exists petitions;")
    cnx.close()


def create_tables():
    create_petitions_table()
    create_tags_table()
    create_petition_tag_table()
    create_log_table()
    create_sentiment_table()
    print('all tables were created')


def drop_tables():
    print("dropping tables...")
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.execute(""" DROP TABLE IF EXISTS log, petition_tag, tags, sentiment, petitions;""")
    cnx.commit()
    cursor.close()
    cnx.close()
    print('all tables were dropped')


def update_tables():
    print("updating petition, log and pet 2 tag tables...")
    # retrieving the list of all petitions from the 'petitions' table
    # each petition in the list has petition.before_adding_tags attribute
    list_of_petitions_to_scrape = get_list_of_petitions_from_petitions_table()
    # scraping each petition in the list of petitions
    # and creating a list of petitions that haven't been tagged yet
    pre_tagged_petitions = []
    deactivated_petitions = []
    batch_petitions = []
    for count, pet in enumerate(list_of_petitions_to_scrape):
        try:
            print("\tRetrieving data for petition #%d: %s" % (count + 1, pet.title))
        except UnicodeEncodeError:
            print(b"\tRetrieving data for petition #%d: %s".decode() % (count + 1, pet.title.encode(errors='ignore')))
        try:
            pet.run()
            batch_petitions.append(pet)
            if pet.status_code == 404:
                deactivated_petitions.append(pet.id)
            elif pet.before_adding_tags:
                print("before adding tags")
                print("\tWill add tags...")
                pre_tagged_petitions.append(pet)
            if count % 1 == 0:
                print('insert rows to the "petition_rows" table')
                try:
                    insert_into_petition_tag_table(pre_tagged_petitions)
                    print("\tBatch done!")
                    print('------------------------------------------------------------')
                except:
                    print('-----------------------error-----------------------')
                    print('failed to update tag info')
                list_of_scraped_petitions = list_of_petitions_to_scrape
                # adding only the pre-tagged petitions into the petition_tag table
                # in addition to inserting the petitions and the corresponding
                # tags to the petition_tag table, this function will update the 'petitions' table
                # and the pet.before_adding_tags attribute

                # and finally
                try:
                    insert_into_log_table(batch_petitions)
                finally:  # only when batch size =1
                    batch_petitions = []

                try:
                    update_petition_content(pre_tagged_petitions)
                finally:  # only when batch size =1
                    pre_tagged_petitions = []
            if deactivated_petitions:
                deactivate_petitions(deactivated_petitions)
        except Exception as e:
            print('-----------------------error-----------------------')
            print(e)
            batch_petitions = []
    print("\tALL done!")


if __name__ == '__main__':
    main()
