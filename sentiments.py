import mysql.connector


def create_sentiment_table():
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.execute("""  CREATE TABLE IF NOT EXISTS sentiment (
                            id INT auto_increment,
                            petition_id INT,
                            score_tag VARCHAR(100),
                            agreement VARCHAR(100),
                            subjectivity VARCHAR(100),
                            confidence INT,
                            irony VARCHAR(100),
                            FOREIGN KEY(petition_id) REFERENCES petitions(id),
                            PRIMARY KEY (id)
                            );""")

    cnx.commit()
    cursor.close()
    cnx.close()