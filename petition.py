from datetime import datetime as dt

import bs4
import requests as r
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.options import Options

base_url = r"https://www.thepetitionsite.com/%s"
browse_url = base_url % "browse-petitions/#%s"


class Petition:
    """ The petition class handles the extraction of the relevant data from the url supplied to it"""

    def __init__(self, url, title=None, before_adding_tags=None, petition_id=None, content=None):
        self.url = url
        self.title = title
        self.content = content
        self.id = petition_id
        self.before_adding_tags = before_adding_tags
        self.driver = None
        self.goal = None
        self.supporters = None
        self.categories = []
        self.author = None  # future feature
        self.location = None  # future feature
        self.recipient = None  # future feature
        self.created = dt.utcnow()
        self.status_code = None

    def get_elements(self, url):
        """ fetches page by url and extracts required html elements.
        handles case of non existing element """
        self.driver.get(url)
        element_ids = ("petition-col", "categorylinks")
        elements = {}
        for element_id in element_ids:
            try:
                elements[element_id] = self.driver.find_element_by_id(element_id)
            except NoSuchElementException:
                print('element "%s" not found in page, returning None.' % element_id)
                elements[element_id] = None
        return elements

    @staticmethod
    def pre_strip_data(elements):
        """ organize data required for extracting the metrics from their corresponding elements """
        # TODO: add author
        return {"supporters": {"id": "div", "class": "progress__supporters", "elem": elements["petition-col"]},
                "goal": {"id": "div", "class": "progress__goal", "elem": elements["petition-col"]},
                "categories": {"id": "a", "class": "categoryForPixel", "elem": elements["categorylinks"]},
                "content": {"id": "div", "class": "overview hide_from_unsigned overview--open",
                            "elem": elements["petition-col"]},
                "content_more": {"id": "div", "class": "overview hide_from_unsigned",
                                 "elem": elements["petition-col"]}
                }

    @staticmethod
    def strip_element(elem, id, class_name):
        """ extracts tags from provided element  """
        try:
            html = elem.get_attribute('innerHTML')
            bs_html = bs4.BeautifulSoup(html, features="html5lib")
            return [e.text for e in bs_html.find_all(id, class_=class_name)]
        except AttributeError:
            return None

    @staticmethod
    def extract_numeric(data_str):
        """ extrects a numeric value from messy data """
        return int(data_str[0].split()[0].replace(",", ""))

    def strip_elements(self, pre_stripped):
        """ applies the strip_element function for each metric """
        stripped = {}
        for key, value in pre_stripped.items():
            stripped[key] = self.strip_element(value['elem'],
                                               value['id'],
                                               value['class'])
        return stripped

    def sanitize_stats(self, stats_dict):
        """ cleaning metrics from unnecessary characters and converting to in when data is numeric"""

        self.goal = self.extract_numeric(stats_dict['goal']) if stats_dict['goal'] else None
        self.supporters = self.extract_numeric(stats_dict['supporters']) if stats_dict['supporters'] else None
        if stats_dict['content']:
            self.content = stats_dict['content'][0]
        else:
            self.content = stats_dict['content_more'][0][:-4]
        self.categories = stats_dict['categories']

    def run(self, headless=True):
        """ completes the cycle from raw data fetching to processed data """
        options = Options()
        if headless:
            options.add_argument("--headless")

        print("starting driver")
        with webdriver.Firefox(options=options) as driver:
            try:
                status_code = r.get(self.url, headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
                                    ).status_code
                if status_code == 200:
                    self.driver = driver
                    elements = self.get_elements(self.url)
                    pre_stripped = self.pre_strip_data(elements)
                    stats_dict = self.strip_elements(pre_stripped)
                    self.sanitize_stats(stats_dict)
                else:
                    print("page returned with status code %d " % status_code)
                self.status_code = status_code
            except Exception as e:
                raise e


urls = ["https://www.thepetitionsite.com/734/643/772/can-a-white-supremacist-be-trusted-to-save-the-lives-of-those-he-hates"]

petitions = []
for url in urls:
    petitions.append(Petition(url))
    petitions[-1].run()
