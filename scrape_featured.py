import re

import bs4
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from petition import Petition, base_url, browse_url


def fetch_featured(url, headless=True):
    """ retrieve a site's html using selenium and extracting the feed petitions element """

    options = Options()
    if headless:
        options.add_argument("--headless")

    with webdriver.Firefox(firefox_options=options) as driver:
        driver.get(url)
        elem = driver.find_element_by_id("%s_petitions" % url.split('#')[-1])
        return elem.get_attribute('innerHTML')


def get_petition_list(content):
    """ extracts relative url per petition from html string """
    bs = bs4.BeautifulSoup(content, features="html5lib")
    return bs.findAll("div", attrs={"class": "petition"})


def extract_relative_url(petition):
    """ extracts the relative url for a petition from the scraped petition list """
    petition_links = petition.findAll("a")
    distinct_links = set([a.attrs['href'] for a in petition_links])
    if len(distinct_links) > 1:
        raise ValueError("There should only be one unique relative url per petition")
    relative_url = tuple(distinct_links)[0]
    return relative_url


def check_petition_url(relative_url):
    """ checks if the extracted url is in the required format (two are possible) """
    url_pattern = re.compile("((/\d+){3}/(\w+-?)+)|/takeaction/\d+/\d+/\d+/")
    match = url_pattern.search(relative_url)
    return match.group() if match else None


def extract_titles(petition_list):
    """ iterates over petition html elements and extract title """
    return [petition.findAll('a')[1].text for petition in petition_list]


def get_relative_urls(petition_list):
    """ returns a list of relative urls for all petitions in the scraped page """
    relative_urls = []
    failed = []
    valid_petitions = []
    while petition_list:
        petition = petition_list.pop(0)
        relative_url = extract_relative_url(petition)
        if check_petition_url(relative_url):
            relative_urls.append(relative_url)
            valid_petitions.append(petition)
        else:
            failed.append(petition)
    if failed:
        print("The following failed:")
        print(failed)
    return relative_urls, valid_petitions


def collect_featured(feed_name="hottest", limit=None, headless=True):
    """ fetch all the metrics for the petitions in the #hottest/#nonprofit/#activist section """
    print('Fetching main page "%s"...' % feed_name.lower())
    content = fetch_featured(browse_url % feed_name.lower(), headless=headless)
    print("Extracting petition list...")
    petition_list = get_petition_list(content)
    print("Extracting titles...")
    print("Extracting petition urls...")
    limit = limit if limit and isinstance(limit, int) else len(petition_list)
    relative_urls, petition_list = get_relative_urls(petition_list)[:limit]
    titles = extract_titles(petition_list)[:limit]

    return ((relative_url, title) for relative_url, title in zip(relative_urls, titles))


def init_petitions(site_petitions):
    """ generate a Petition object for each petition url """
    petition_list = []
    for count, (relative_url, title) in enumerate(site_petitions):
        try:
            print("\tInitializing petition #%d: %s" % (count + 1, title))
        except UnicodeEncodeError:
            print(b"\tInitializing petition #%d: %s".decode() % (count + 1, title.encode(errors='ignore')))
        petition_list.append(Petition(base_url % relative_url, title))
        # yield Petition(base_url % relative_url, title)
    return petition_list


def run_petitions(petitions):
    for count, petition in enumerate(petitions):
        try:
            print("\tfetcing petition #%d: %s" % (count + 1, petition.title))
        except UnicodeEncodeError:
            print(b"\tFetching petition #%d: %s".decode() % (count + 1, petition.title.encode(errors='ignore')))
        petition.run()
        yield petition
