import urllib.parse
from time import sleep
import mysql
import requests as r


def post_request(txt, api_key="9aae4370db99e51d90e41454022092d0", lang="en", verbose="y"):
    params = {"key": api_key,
              'lang': lang,
              "verbose": verbose,
              "txt": txt,
              "doc": 'undefined',
              "rt": 'n',
              "txtf": 'plain',
              "uw": 'n',
              "egp": 'n',
              "dm": 's',
              "sdg": 'l'
              }

    encoded_params = urllib.parse.urlencode(params)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    base_url = f"https://api.meaningcloud.com/"
    endpoint = "sentiment-2.1"
    response = r.post(f"{base_url}{endpoint}", data=encoded_params, headers=headers)

    if response.status_code == 200:
        return response.json()
    else:
        raise ValueError(f"ERROR!\n{response.status_code}")


def get_content():
    query = "select p.id, p.title, p.content from petitions as p left join sentiment as s on p.id = s.petition_id " \
            "where content is not null and s.id is null;"
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.execute(query)
    for row in cursor.fetchall():
        yield row


def make_insert_query(fields):
    placeholders = ["%s"] * (len(fields) + 1)
    return f"insert into sentiment (petition_id, {', '.join(fields)}) values ({', '.join(placeholders)})"


def insert_sentiment(data):
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.executemany(data[0], data[1])
    cnx.commit()


def order_fields(result, fields):
    return [result[field] for field in fields]


def main():
    # don't over use, there is a limited amount of credits
    txts = get_content()
    inserts = []
    fields = ['score_tag', 'agreement', 'subjectivity', 'confidence', 'irony']
    for txt in txts:
        all_txt = txt[1] + "\n\n" + txt[2]
        result = post_request(all_txt, verbose='n')
        values = order_fields(result, fields)
        inserts.append([txt[0]] + values)
        sleep(1)

    data = (make_insert_query(fields), inserts)
    insert_sentiment(data)


if __name__ == '__main__':
    main()
