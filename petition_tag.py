import mysql.connector


def create_petition_tag_table():
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    cursor.execute("""  CREATE TABLE IF NOT EXISTS petition_tag (
                            id INT auto_increment,
                            petition_id INT,
                            tag_id INT,
                            petition_title VARCHAR(200),
                            tag_name VARCHAR(40),
                            FOREIGN KEY(petition_id) REFERENCES petitions(id),
                            FOREIGN KEY(tag_id) REFERENCES tags(id),
                            PRIMARY KEY (id)
                            );""")

    cnx.commit()
    cursor.close()
    cnx.close()


def insert_into_petition_tag_table(petitions_list):
    """
    This function will receive a list of petitions and it will insert to the petition_tag table
    NOTICE: this function is suppose to only receive petitions from the 'petitions' table that their
    'before_first_scrape' column is TRUE, so that each petition will be inserted only once
    """
    cnx = mysql.connector.connect(user='root', database='petitions', password='Roey!123',
                                  auth_plugin='mysql_native_password')
    cursor = cnx.cursor()
    for petition in petitions_list:
        cursor.execute("""  SELECT id, title FROM petitions
                            WHERE title = %s;""", (petition.title,))
        print(petition.categories)
        petition_id = cursor.fetchall()[0][0]

        for tag in petition.categories:
            if tag == 'Media, Arts, Culture':
                tag = 'Media,Arts, Culture'
            cursor.execute("""  SELECT id FROM tags
                                WHERE tag = %s;""", (tag,))
            print(tag)
            tag_id = cursor.fetchall()[0][0]
            cursor.execute("""  INSERT INTO petition_tag
                                (petition_id, tag_id, petition_title, tag_name)
                                VALUES (%s, %s, %s, %s);""", (petition_id, tag_id, petition.title, tag))
        # update before_frist_scrape column in 'petitions' to False
        cursor.execute("""  UPDATE petitions
                            SET before_adding_tags = FALSE
                            WHERE id = %s;""", (petition_id,))
        cnx.commit()
        petition.before_adding_tags = False
    cnx.commit()
    cursor.close()
    cnx.close()
